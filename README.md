# Noroff Task 27

Task 27: PGM online

� Create a Web Api from an empty ASP.NET Core Application
which allows for clients to get all the current supervisors in
a DB as JSON

� It must use Entity Framework code first

� The supervisor class must only have a maximum of two
properties (It should also have an Id) Therefore, total of
three

______________________________

Task 28: PGM online CRUD API

� Update the Web Api from task 27 to allow for CRUD
operations