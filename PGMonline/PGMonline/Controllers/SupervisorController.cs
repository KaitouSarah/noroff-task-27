﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PGMonline.Models;

namespace PGMonline.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {
        private readonly SupervisorContext _context;

        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetSupervisors()
        {
            return _context.Supervisors;
        }

        [HttpPost]
        public ActionResult<Supervisor> addSupervisor(Supervisor supervisor)
        {
            _context.Supervisors.Add(supervisor);
            _context.SaveChanges();

            return CreatedAtAction("GetSupervisorById", new Supervisor { Id = supervisor.Id }, supervisor);
        }

        [HttpGet("{id}")]
        public ActionResult<Supervisor> GetSupervisorById(int id)
        {
            var supervisor = _context.Supervisors.Find(id);

            return supervisor;
        }

        [HttpPut("{id}")]
        public ActionResult<Supervisor> UpdateSupervisor(int id, Supervisor supervisor)
        {
            if (id != supervisor.Id)
            {
                return BadRequest();
            }

            _context.Entry(supervisor).State = EntityState.Modified;
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult<Supervisor> DeleteSueprvisor(int id)
        {
            var supervisor = _context.Supervisors.Find(id);

            if (supervisor == null)
            {
                return NotFound();
            }

            _context.Supervisors.Remove(supervisor);
            _context.SaveChanges();

            return supervisor;
        }
    }
}