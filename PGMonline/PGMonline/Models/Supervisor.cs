﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PGMonline.Models
{
    public class Supervisor
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Level { get; set; }

        public string SerializeSupervisor()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
